public class Student {
	
	private int id ;
	private String name ;
	private String gender;
	private int card_id;
	public String generation;


	public Student(int id, String name, String gender, int card_id, String generation) {
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.card_id = card_id;
		this.generation = generation;
	}


	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public int getCard_id() {
		return card_id;
	}
	
	public void setCard_id(int card_id) {
		this.card_id = card_id;
	}
	
	public String getGeneration() {
		return generation;
	}
	
	public void setGeneration(String generation) {
		this.generation = generation;
	}
}
