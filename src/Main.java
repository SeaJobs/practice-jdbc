import java.sql.*;
import java.util.Scanner;

public class Main {
    static Scanner Input = new Scanner(System.in);

    public static void display(Connection connection) {

        try {
            //3- String stSelect
            String stSelect = "SELECT * FROM student ";
            PreparedStatement ps = connection.prepareStatement(stSelect);
            //4-Execute Statement
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                System.out.println("ID          : " + resultSet.getInt("ID"));
                System.out.println("Name        : " + resultSet.getString("Name"));
                System.out.println("Gender      : " + resultSet.getString("Gender"));
                System.out.println("Card_ID     : " + resultSet.getInt("Card_ID"));
                System.out.println("Generation  : " + resultSet.getString("Generation"));
            }

            resultSet.close();
            ps.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void add(Connection connection){


        System.out.print("Input ID : ");
        int StID = Input.nextInt();
        System.out.print("Input StName : ");
        String StName = Input.next();
        System.out.print("Input Gender : ");
        String StGen = Input.next();
        System.out.print("Input Card_ID :");
        int StCard_ID = Input.nextInt();
        System.out.print("Input Generation :");
        String  StGeneration = Input.next();

        try{
            String stInsert = "INSERT INTO student (id, name, gender, card_id, generation) VALUES (?,?,?,?,?)";

            PreparedStatement ps = connection.prepareStatement(stInsert);
            ps.setInt(1, StID);
            ps.setString(2, StName);
            ps.setString(3, StGen);
            ps.setInt(4, StCard_ID);
            ps.setString(5, StGeneration);

            int rowUpdated = ps.executeUpdate();
            System.out.println(rowUpdated);
            System.out.println("Inserted Successfully !");
            
            ps.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void update(Connection connection) throws SQLException {
        try  {
            System.out.println("Enter ID to Update : ");
            int UserID = Input.nextInt();

            String qslFound = " SELECT * FROM student WHERE id =" + UserID;
            Statement st = connection.createStatement();
            st.execute(qslFound);
            ResultSet resultSet = st.executeQuery(qslFound);
            if (resultSet != null) {
                try{
                    System.out.print("Input New ID : ");
                    int StID = Input.nextInt();
                    System.out.print("Input New StName : ");
                    String StName = Input.next();
                    System.out.print("Input new Gender : ");
                    String StGen = Input.next();
                    System.out.print("Input new Card_ID :");
                    int StCard_ID = Input.nextInt();
                    System.out.print("Input new Generation :");
                    String  StGeneration = Input.next();
                    String stUpdate = "UPDATE student SET id = " + StID +
                            ", name = " +"'"+ StName +"'"+
                            ", gender = " +"'"+ StGen +"'"+
                            ", card_id = " + StCard_ID +
                            ", generation = " + StGeneration +
                            " WHERE id = " + UserID;
                    Statement st1 = connection.createStatement();
                    st1.executeUpdate(stUpdate);
                    System.out.println("Updated Successfully !");
                    st1.close();
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                return;
            }
        }catch ( SQLException e ){
            e.printStackTrace();
        }
    }

    public static void delete( Connection connection) throws SQLException {
        System.out.println("Enter ID to Delete : ");
        int UserID = Input.nextInt();
        System.out.print("Do you want to delete it?\t");
        System.out.println("Press N[no] to cancel / Press Y[yes] to delete");
        String op = Input.next();
        String  yes ="y";
        String no = "n";
        if(op.compareToIgnoreCase(yes)==0){
            String stDelete = "DELETE FROM student where id = 12";
            Statement ps = connection.createStatement();
            ps.executeUpdate(stDelete);
            System.out.println("Successful to deleted !");
        }
        else return;
    }

    public static void main(String[] args) throws SQLException {
        while (true) {
            ConnectionDB connection = new ConnectionDB();
            Connection connection1 = connection.getConnectionDB();
            System.out.println(" Choose Option : => 1/ Display  | 2/ Add    | 3/Update    | 4/ Delete");
            System.out.print("Enter Option : ");
            int Option = Input.nextInt();

            switch (Option) {
                case 1 -> {
                    display(connection1);
                }
                case 2 -> {
                    add(connection1);
                }
                case 3 -> {
                    update(connection1);
                }
                case 4 -> {
                    delete(connection1);
                }
                default -> {
                    System.out.println("Invalid Input");
                }
            }
        }
    }
}
